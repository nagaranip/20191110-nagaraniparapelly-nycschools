//
//  NYCSchoolsViewModelObject.swift
//  20191110-NagaraniParapelly-NYCSchools
//
//  Created by nagaranik on 11/10/19.
//  Copyright © 2019 nagaranik. All rights reserved.
//

import UIKit

@objc class NYCSchoolsViewModelObject: NSObject {

    /** Alias for existing type ***/
    typealias completionBlock = (Bool,Int)->()
    /** Creating instance of network class to call api ***/
    private let networkClassObject = NetworkRequestObject()
    /** To append school info data ***/
    private var schoolInfoList = [SchoolInfo]()
    /** To append data to search filter array ***/
    private var filtered = [SchoolInfo]()
    
    /*** call network api and based on responce send back to VC to update UI ***/
    func getSchoolDataFromNetworkAPI(urStr:String,completionBlock:@escaping completionBlock)
    {
        networkClassObject.callNetworkAPI(urlString: urStr) { (json, success,code) in
            if success
            {
                for element in json!{
                    
                    let schoolObj:SchoolInfo = SchoolInfo.init(name: element["school_name"] as? String ?? "", email: element["school_email"] as? String ?? "", contact: element["phone_number"] as? String ?? "", dbn: element["dbn"] as? String ?? "")
                    
                    self.schoolInfoList.append(schoolObj)
                }
            }
            completionBlock(success,code)
        }
    }
    /** Based on newtork responce update UI by showing alert messages ***/
    func getInfoText(isFromNetwork:Bool)->String
    {
        if isFromNetwork
        {
            return ConnectionError
        }
        return NoResults
    }
    /*** Alert messages based on error code ***/
    func getAlertInfo(code:Int)->(String,String)
    {
        if code == -1009
        {
            return (AlertITitle,CheckNetwork)
        }
        else{
            return (AlertServerTitle,ServerErr)
        }
    }
    /*** Table view number or rows based on array count / search filter count ***/
    func numberOfItemsInRow()->Int
    {
        if filtered.count > 0
        {
            return filtered.count
        }
        return schoolInfoList.count
    }
    /*** school name to display in each row***/
    func titleForItemIndexPath(indexpath:IndexPath)->String
    {
        if filtered.count > 0
        {
            return filtered[indexpath.row].name
        }
        return schoolInfoList[indexpath.row].name
    }
    /*** Getting selected row dbn to pass to get SAT details ***/
    func didSelectItem(indexPath:IndexPath)->SchoolInfo
    {
        
        if filtered.count > 0
        {
            return filtered[indexPath.row]
        }
        return schoolInfoList[indexPath.row]
    }
    /*** Based on search bar search test will filter ***/
    func searchText(stext:String)
    {
        
        filtered = schoolInfoList.filter({ (schoolObj) -> Bool in
            
            let tmp:NSString = schoolObj.name as NSString
            let range = tmp.range(of: stext, options: NSString.CompareOptions.caseInsensitive)
            return range.location != NSNotFound
            
        })
        
    }
}
