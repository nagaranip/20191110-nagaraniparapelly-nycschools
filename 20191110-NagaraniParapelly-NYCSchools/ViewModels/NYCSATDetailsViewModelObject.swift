//
//  NYCSATDetailsViewModelObject.swift
//  20191110-NagaraniParapelly-NYCSchools
//
//  Created by nagaranik on 11/10/19.
//  Copyright © 2019 nagaranik. All rights reserved.
//

import UIKit

class NYCSATDetailsViewModelObject: NSObject {
    typealias completionBlock = ()->()
    
    /*** Creating instance to call network api ***/
    private  let networkClassObject = NetworkRequestObject()
    /** Creating instance to store SAT data **/
    private var schoolSATDaataObject:SchoolSATResults?
    func getSchoolDataFromNetworkAPI(urStr:String,completionBlock:@escaping completionBlock)
    {
        /** After calling network based on responce will parse or update UI **/
        networkClassObject.callNetworkAPI(urlString: urStr) { (json,success,code) in
            
            if success{
                for element in json!{
                    
                    let schoolSATObj:SchoolSATResults = SchoolSATResults.init(count: element["num_of_sat_test_takers"] as? String ?? "0", rScore: element["sat_critical_reading_avg_score"] as? String ?? "0", wScore: element["sat_writing_avg_score"] as? String ?? "0", mScore: element["sat_math_avg_score"] as? String ?? "0")
                    self.schoolSATDaataObject = schoolSATObj
                }
                completionBlock()
            }
        }
    }
    /** Getting SAT match avg score ***/
    func getMathScore()->String
    {
        return (schoolSATDaataObject?.matScore) ?? NotAvaible
    }
    /** Getting SAT Avg reading score ***/
    func getReadingScore()->String
    {
        return schoolSATDaataObject?.readingScore ?? NotAvaible
    }
    /** Getting writing avg score ***/
    func getWritingScore()->String
    {
        return schoolSATDaataObject?.writingScrore ?? NotAvaible
    }
    /*** Formatting email to clickable link ***/
    func getSchoolClikable(text:String?)->NSAttributedString
    {
        if  text != nil
        {
            let range = (text! as NSString).range(of: text!, options: .caseInsensitive)
            
            let attributeStr:NSMutableAttributedString = NSMutableAttributedString(string: text!)
            
            attributeStr.addAttribute(.link, value: text!, range:range )
            
            return attributeStr
        }
        return NSAttributedString(string: "Info Not Available")
    }
    
    /***  Formatting mail clickable **/
    func getMailFormat(text:String?)->String
    {
        return text ?? NotAvaible
        
    }
    /*** Formatting phone number ***/
    func call(phoneNumber:String?)->URL? {
        let cleanPhoneNumber = phoneNumber?.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
        let urlString:String = "tel://\(cleanPhoneNumber ?? "")"
        if let phoneCallURL = URL(string: urlString) {
            print("phoneCallURL\(phoneCallURL)")
            return phoneCallURL
        }
        return nil
    }
}
