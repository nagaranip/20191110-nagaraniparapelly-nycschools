//
//  NYCSchoolSATDetailsModelObject.swift
//  20191110-NagaraniParapelly-NYCSchools
//
//  Created by nagaranik on 11/10/19.
//  Copyright © 2019 nagaranik. All rights reserved.
//

import Foundation

/*** schoolSATResults struct to store SAT data ***/
struct SchoolSATResults
{
    let testCount:String
    let readingScore:String
    let matScore:String
    let writingScrore:String
    
    init(count:String,rScore:String,wScore:String,mScore:String) {
        
        self.testCount = count
        self.readingScore = rScore
        self.matScore = mScore
        self.writingScrore = wScore
    }
}
