//
//  SchoolInfoModelObject.swift
//  20191110-NagaraniParapelly-NYCSchools
//
//  Created by nagaranik on 11/10/19.
//  Copyright © 2019 nagaranik. All rights reserved.
//

import Foundation

/*** School Info struct to store school info ***/
public struct SchoolInfo
{
    let name:String
    let email:String
    let contact:String
    let dbn:String
    init(name:String,email:String,contact:String,dbn:String) {
        self.name = name
        self.email = email
        self.contact = contact
        self.dbn = dbn
    }
    
}
