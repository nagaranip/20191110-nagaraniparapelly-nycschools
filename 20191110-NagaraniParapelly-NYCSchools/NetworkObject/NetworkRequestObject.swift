//
//  NetworkRequestObject.swift
//  20191110-NagaraniParapelly-NYCSchools
//
//  Created by nagaranik on 11/10/19.
//  Copyright © 2019 nagaranik. All rights reserved.
//

import UIKit

class NetworkRequestObject: NSObject {

    /** Alias for an existing closure  ***/
    typealias completionBlock = ([[String:Any]]?, Bool,Int)->()
    
    /*** Call network api to get respective responce and once we receive responce . Escaping closure will execute ***/
    
    func callNetworkAPI(urlString:String,completionBlock:@escaping completionBlock)
    {
        /*** Session to execute data task to endpoint indicated by url.Once Task finished completion block will execute ***/
        let session = URLSession.shared
        let url = URL(string: urlString)!
        let task = session.dataTask(with: url) { data, response, error in
            if error as NSError? != nil || data == nil {
                completionBlock(nil,false,error!._code)
                return
            }
            
            /** Once it success responce will send json to respective view models for parsing json***/
            if let httpResponse = response as? HTTPURLResponse {
                print("responce code\(httpResponse.statusCode)")
                if httpResponse.statusCode == 200
                {
                    do {
                        let json:[[String:Any]] = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [[String:Any]] ?? []
                        completionBlock(json,true,httpResponse.statusCode)
                        
                    } catch {
                        print("JSON error: \(error.localizedDescription)")
                    }
                }
                else{
                    completionBlock(nil,false,httpResponse.statusCode)
                }
            }
        }
        
        /** Execute task ***/
        task.resume()
    }
}
