//
//  main.m
//  20191110-NagaraniParapelly-NYCSchools
//
//  Created by nagaranik on 11/10/19.
//  Copyright © 2019 nagaranik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
