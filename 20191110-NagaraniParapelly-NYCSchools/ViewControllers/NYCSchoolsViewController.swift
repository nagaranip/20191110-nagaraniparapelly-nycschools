//
//  NYCSchoolsViewController.swift
//  20191110-NagaraniParapelly-NYCSchools
//
//  Created by nagaranik on 11/10/19.
//  Copyright © 2019 nagaranik. All rights reserved.
//

import UIKit

let CellReUse = "cellReUse"

@objc class NYCSchoolsViewController: NYCRootViewController {

    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    private var schoolInfoObject:SchoolInfo!
    private var viewModelObj = NYCSchoolsViewModelObject()
    override func viewDidLoad() {
        super.viewDidLoad()
      /** Registering custom cell xib ***/
        tableView.register(UINib(nibName: kNYCCustomCellXIB, bundle: nil), forCellReuseIdentifier: CellReUse)
        /*** search delagte to self to trigger search callbacks ***/
        searchBar.delegate = self
        /** Hiding the table untill we recieve success responce ***/
        tableView.isHidden = true
        /*** Hiding label untill we get responce from network api***/
        infoLabel.isHidden = true
        
        DispatchQueue.main.async {
            self.startAnimating()
        }
        viewModelObj.getSchoolDataFromNetworkAPI(urStr: kGetNYCList ) { (success,code) in
            if success
            {
                DispatchQueue.main.async {
                    self.updateUI()
                }
            }
            else{
                 DispatchQueue.main.async { [unowned self] in
                    self.updateErrorUI(code: code)
                }
            }
        }
    }
    /** Updating UI Based on error ***/
    func updateErrorUI(code:Int)
    {
        self.stopAnimating()
        self.infoLabel.text = self.viewModelObj.getInfoText(isFromNetwork: true)
        self.infoLabel.isHidden = false
        let alertInfo:(String,String) =  self.viewModelObj.getAlertInfo(code: code)
        let alert = UIAlertController(title: alertInfo.0, message: alertInfo.1, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    /*** Update UI based on success responce ***/
    func updateUI()
    {
        self.infoLabel.isHidden = true
        self.tableView.isHidden = false
        self.stopAnimating()
        self.tableView.reloadData()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if let vc:NYCSchoolsSATDetailsViewController = segue.destination as? NYCSchoolsSATDetailsViewController {
            vc.schoolDetailsObject = schoolInfoObject
        }
        
    }

  
}
/** Extension for tableview callback ***/
extension NYCSchoolsViewController:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModelObj.numberOfItemsInRow()
    }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellObj = tableView.dequeueReusableCell(withIdentifier: "cellReUse", for: indexPath) as! CustomTableViewCell
        cellObj.nameLabel.text = viewModelObj.titleForItemIndexPath(indexpath: indexPath)
        return cellObj
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        schoolInfoObject = viewModelObj.didSelectItem(indexPath: indexPath)
        self.performSegue(withIdentifier: kNYCSchoolStoryBoardSegue, sender: nil)
        
    }

}

/*** Extension for search bar callbacks ***/
extension NYCSchoolsViewController:UISearchBarDelegate
{
   
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModelObj.searchText(stext: searchText)
        DispatchQueue.main.async {
             self.tableView.reloadData()
        }
       
    }
    
    
}
