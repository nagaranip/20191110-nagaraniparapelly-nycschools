//
//  NYCRootViewController.h
//  20191110-NagaraniParapelly-NYCSchools
//
//  Created by nagaranik on 11/10/19.
//  Copyright © 2019 nagaranik. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NYCRootViewController : UIViewController
-(void)startAnimating;
-(void)stopAnimating;
@end

NS_ASSUME_NONNULL_END
