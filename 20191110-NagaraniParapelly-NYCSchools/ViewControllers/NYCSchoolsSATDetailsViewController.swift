//
//  NYCSchoolsSATDetailsViewController.swift
//  20191110-NagaraniParapelly-NYCSchools
//
//  Created by nagaranik on 11/10/19.
//  Copyright © 2019 nagaranik. All rights reserved.
//

import UIKit
import MessageUI

class NYCSchoolsSATDetailsViewController: NYCRootViewController,MFMailComposeViewControllerDelegate {
    /** As i need to access in previous VC so not declaring as private ***/
    var schoolDetailsObject:SchoolInfo!
    /** UI Outlets ***/
    @IBOutlet weak var detailsView: UIView!
    @IBOutlet weak var writingScoreLabel: UILabel!
    @IBOutlet weak var readingScoreLabel: UILabel!
    @IBOutlet weak var contactLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mathScoreLabel: UILabel!
    @IBOutlet weak var infoView: UIView!
    /*** Creating instance for calling network API ***/
    private var schoolDetailsViewModelObject = NYCSATDetailsViewModelObject()
    override func viewDidLoad() {
        super.viewDidLoad()
        updateSchoolInfo()
        let satURL = "\(kGetNYCSATDetails)\("/?dbn=")\(schoolDetailsObject.dbn)"
        schoolDetailsViewModelObject.getSchoolDataFromNetworkAPI(urStr: satURL) {
            DispatchQueue.main.async {
                self.updateUI()
            }
        }
    }
    /*** Update school data ***/
    func updateSchoolInfo()
    {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(contactlabelClicked))
        contactLabel.addGestureRecognizer(gesture)
        
        let emailgesture = UITapGestureRecognizer(target: self, action: #selector(emailClickable))
        emailLabel.addGestureRecognizer(emailgesture)
        
    }
    /** Mail compose ***/
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            print("Cancelled")
        case MFMailComposeResult.saved.rawValue:
            print("Saved")
        case MFMailComposeResult.sent.rawValue:
            print("Sent")
        case MFMailComposeResult.failed.rawValue:
            print("Error:)")
        default:
            break
        }
        controller.dismiss(animated: true)
    }
    /*** Email click action ***/
    @objc func emailClickable()
    {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([schoolDetailsViewModelObject.getMailFormat(text: schoolDetailsObject?.email)])
            mail.setMessageBody("<p>Hello!</p>", isHTML: true)
            
            present(mail, animated: true)
        } else {
            
            showError()
        }
    }
    /** Show error email device support***/
    func showError()
    {
        let alertMessage = UIAlertController(title: EmailNotSent, message: DeviceEmailSupport, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title:"OK", style: UIAlertAction.Style.default, handler: nil)
        alertMessage.addAction(action)
        self.present(alertMessage, animated: true, completion: nil)
    }
    /** conatact action **/
    @objc func contactlabelClicked()
    {
        guard UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone else {
            let alertMessage = UIAlertController(title: AlertITitle, message:DevicePhoneCall , preferredStyle: UIAlertController.Style.alert)
            let action = UIAlertAction(title:"OK", style: UIAlertAction.Style.default, handler: nil)
            alertMessage.addAction(action)
            self.present(alertMessage, animated: true, completion: nil)
            return
        }
        UIApplication.shared.open(schoolDetailsViewModelObject.call(phoneNumber: schoolDetailsObject?.contact)!)
        
    }
    /** Updating UI SAT and School Info ***/
    func updateUI()
    {
        infoView.layer.borderWidth = 1
        detailsView.layer.borderWidth = 1
        
        infoView.layer.borderColor = UIColor.white.cgColor
        detailsView.layer.borderColor = UIColor.white.cgColor
        
        infoView.layer.cornerRadius = 10
        detailsView.layer.cornerRadius = 10
        self.nameLabel.text = schoolDetailsObject?.name  ?? ""
        
        self.contactLabel.attributedText = schoolDetailsViewModelObject.getSchoolClikable(text: schoolDetailsObject?.contact)
        
        self.emailLabel.attributedText = schoolDetailsViewModelObject.getSchoolClikable(text: schoolDetailsObject?.email)
        
        self.readingScoreLabel.textColor = UIColor.white
        
        self.mathScoreLabel.text = schoolDetailsViewModelObject.getMathScore()
        
        self.readingScoreLabel.text = schoolDetailsViewModelObject.getReadingScore()
        
        self.writingScoreLabel.text = schoolDetailsViewModelObject.getWritingScore()
        
    }
    
    deinit {
        print("SchoolSAT  VC Deinit")
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
