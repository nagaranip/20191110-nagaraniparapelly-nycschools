//
//  NYCConstants.swift
//  20191110-NagaraniParapelly-NYCSchools
//
//  Created by nagaranik on 11/10/19.
//  Copyright © 2019 nagaranik. All rights reserved.
//

import Foundation

let kNYCSchoolStoryBoardSegue = "NYCSCHOOLSSAT"
let kNYCCustomCellXIB = "CustomTableViewCell"

/*** label constants**/

let NotAvaible = "Not Available"

let ConnectionError = "Connectivity Error"

let NoResults = "No Resuls Found"

let CheckNetwork = "Check Internet connection"

let ServerErr = "Please tryagain later"

let AlertITitle = "Alert"

let AlertServerTitle = "Server Error"

let EmailNotSent = "Could not sent email"
let DeviceEmailSupport = "Check if your device have email support!"
let DevicePhoneCall =  "Device does not support phone calls."
