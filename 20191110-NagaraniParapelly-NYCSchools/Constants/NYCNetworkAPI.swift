//
//  NYCNetworkAPI.swift
//  20191110-NagaraniParapelly-NYCSchools
//
//  Created by nagaranik on 11/10/19.
//  Copyright © 2019 nagaranik. All rights reserved.
//

import Foundation

let kGetNYCList = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"

let kGetNYCSATDetails = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
